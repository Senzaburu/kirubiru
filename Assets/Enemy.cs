﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public int health;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	/*
	void OnCollisionEnter2D(Collision2D col) {
		Debug.Log("pang");
		if (col.gameObject.tag == "PlayerAttack") {
			Destroy(gameObject);
		}
	}
	*/
	void OnTriggerEnter2D(Collider2D collider) {	
		if (collider.gameObject.tag == "PlayerAttack") {
			//Destroy(gameObject);
			health--;
			if (health <= 0)
				Destroy(transform.parent.gameObject);
		}
	}
	
}

﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

	//Spawn blocks ... + X sections per enemy
	private int ENEMY_MULTIPLIER = 2;
	
	private int BASE_ENEMY_NUMBER = 3;
	
	private int ORIGIN_START = 0;
	private int ORIGIN_CENTER = 0;
	private int ORIGIN_OFFSET = 5;
	
	private float HEALTH_THRESHOLD_THREE = .125f;
	private float HEALTH_THRESHOLD_TWO = .70f;
	
	public GameObject enemyGroup;
	
	
	//Randomize health (weighted 1 most common)

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void SpawnNewEnemy() {
		GameObject spawnedEnemy = (GameObject)Instantiate(enemyGroup, new Vector3(0,0,0), Quaternion.identity);
		Enemy enemyTop = (Enemy)((spawnedEnemy.transform.Find("Enemy Top")).gameObject).GetComponent("Enemy");
		enemyTop.health = ComputeHealth(Random.Range(0f, 1f));
		Enemy enemyMid = (Enemy)((spawnedEnemy.transform.Find("Enemy Mid")).gameObject).GetComponent("Enemy");
		enemyMid.health = ComputeHealth(Random.Range(0f, 1f));
		Enemy enemyBottom = (Enemy)((spawnedEnemy.transform.Find("Enemy Bottom")).gameObject).GetComponent("Enemy");
		enemyBottom.health = ComputeHealth(Random.Range(0f, 1f));
		
		//Set visuals (Sprites)
	}
	
	private int ComputeHealth(float enemyHealthBracket) {
		if (enemyHealthBracket < HEALTH_THRESHOLD_THREE)
			return 3;
		else if (enemyHealthBracket < HEALTH_THRESHOLD_TWO)
			return 2;
		else
			return 1;
	}
}

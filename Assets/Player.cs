﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	private int currentPlane = 0;
	private int START_Y = 1;
	private int Y_CHANGE = 5;
	
	public bool jumping = false;
	public bool blocking = false;
	public bool blockingTriggered = false;
	
	public GameObject sword;
	public GameObject block;

	// Use this for initialization
	void Start () {
	
	}
	
	float y;
	float x;
	
	float baseDistance = 0f;
	Vector3 baseVector = new Vector3(-6f,1f,0f);
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Vertical")) {
			if (!jumping) {
				y = Input.GetAxisRaw("Vertical");
				
				if (y < 0) {
					if (currentPlane != -1)
						currentPlane--;
				} else if (y > 0) {
					if (currentPlane != 1)
						currentPlane++;
				}
				
				transform.localPosition = new Vector3(transform.localPosition.x, START_Y + currentPlane * Y_CHANGE, transform.localPosition.z);
			}
		}
		
		if (Input.GetButton("Horizontal")) {
			x = Input.GetAxisRaw("Horizontal");
			if (x > 0) {
				if (!jumping) {
					jumping = true;
					rigidbody2D.AddForce(new Vector2(1750f, 0f));
					gameObject.layer = 0;
				}
			}
			if (x < 0 && !blockingTriggered) {
				blocking = true;
				block.SetActive(true);
				blockingTriggered = true;
			}
		} else {
			blocking = false;
			block.SetActive(false);
			blockingTriggered = false;
		}
		
		if (Input.GetButtonDown("Fire1")) {
			sword.SetActive(true);
		}
	}
	
	void OnCollisionEnter2D(Collision2D col) {
		if (col.gameObject.tag == "EnemyGroup") {
					
			if (blocking) {
				//col.gameObject.rigidbody2D.AddForce(new Vector2(100000f, 0f));
				return;
			}
			
			if (!jumping)
				gameObject.layer = 9;
				
			if (gameObject.tag == "PlayerAttack") {
			
			}
			
		}
		if (col.gameObject.tag == "PlayerBase") {
			jumping = false;
			//gameObject.layer = 9;
		}
		
	}
	
	void OnCollisionStay2D(Collision2D col) {
		if (col.gameObject.tag == "EnemyGroup") {
			baseDistance = Vector3.Distance(transform.position,baseVector);
			Debug.Log(baseDistance);
			if (baseDistance < .020f)
				gameObject.layer = 9;
		}
	}
	
}

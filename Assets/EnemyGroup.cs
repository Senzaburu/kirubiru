﻿using UnityEngine;
using System.Collections;

public class EnemyGroup : MonoBehaviour {

	public Player player;
	public GameObject block;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		int childCount = 0;
		foreach (Transform child in transform) {
			childCount++;
		}
		if (childCount == 0)
			Destroy(gameObject);
	}
	
	void OnTriggerEnter2D(Collider2D collider) {	
		if (collider.gameObject.tag == "PlayerDefend") {
			rigidbody2D.AddForce(new Vector2(50000f, 0f));
			player.blockingTriggered = false;
			block.SetActive(false);
		}
	}
	
}
